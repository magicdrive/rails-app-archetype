# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsAppArchetype::Application.config.secret_key_base = 'eb4988fe430956e8fe5639d159892b171650637412dadb06de4c511ba9e61db0073999e544f6d7637358bae149326f2f0a88f4ee361afb4fab70634d07a041f9'
