#! /bin/sh

project_home=$(cd $(dirname $0)/../ && pwd)

cd ${project_home}

exec bundle install --path=${project_home}/vendor/bundle --binstubs=${project_home}/.bundle/binstubs "$@"

exit 0;
