# vim: ft=sh

#
# ruby project environments
#
# Usage: source <this-file>
#

export RAILS_ROOT=$(cd $(dirname $0)/../ && pwd)
export RUBYLIB=${RAILS_ROOT}/lib:$(find ${RAILS_ROOT}/vendor/bundle -name 'lib' -type d | ruby -pne '$_.sub!(/\n/, ":")')

case $(ruby -e 'puts RUBY_VERSION') in
    1.9*)
        bundle_dir=1.9.1
        ;;
    2.0*)
        bundle_dir=2.0.0
        ;;

esac
export PATH=${RAILS_ROOT}/bin:${RAILS_ROOT}/vendor/bundle/ruby/${bundle_dir}/bin:${PATH}

alias cdproject="cd ${RAILS_ROOT}"

PROMPT="%{$fg_bold[red]%}Ruby%{$reset_color%}:$PROMPT"
